<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class HistoryClearCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'history:clear';

    /**
     * @var string
     */
    protected $description = 'Clear saved history';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(): void
    {
        $this->clear();
    }

    public function clear(){
        echo nl2br("History cleared!");
    }
}
