<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;

class PowCommand extends Command
{
    /**
     * @var string
     */
    protected $signature = 'pow';

    /**
     * @var string
     */
    protected $description = 'Exponent all given Numbers';

    public function __construct()
    {
        parent::__construct();
        $commandVerb = $this->getCommandVerb();
        $this->addArgument('base', InputArgument::REQUIRED, 'The base number');
        $this->addArgument('exp', InputArgument::REQUIRED, 'The exponent number');

        $this->signature = sprintf(
            '%s {numbers* : The numbers to be %s}',
            $commandVerb,
            $this->getCommandPassiveVerb()
        );
        $this->description = sprintf('%s all given Numbers', ucfirst($commandVerb));
    }

    protected function getCommandVerb(): string
    {
        return 'Exponent';
    }

    protected function getCommandPassiveVerb(): string
    {
        return 'pow';
    }

    public function handle(): void
    {
        $base = $this->argument('base');
        $exp =  $this->argument('exp');
        $description = $this->generateCalculationDescription($base, $exp);
        $result = $this->calculateAll($base, $exp);

        $this->comment(sprintf('%s = %s', $description, $result));
    }

    protected function generateCalculationDescription($base, $exp)
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);
        $result = $base + $glue + $exp;
        return $result;
    }

    protected function getOperator(): string
    {
        return '^';
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll($base, $exp)
    {
        return $this->calculate($base, $exp);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($base, $exp)
    {
        return pow($base, $exp);
    }
}
